import tkinter as tk
from tkinter import messagebox
class Demo1:
    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.button1 = tk.Button(self.frame, text = 'Register', width = 25, command = self.Register)
        self.button1.pack()
        self.frame.pack()
    def Register(self):
        self.newWindow = tk.Toplevel(self.master)
        self.app = KidsRegistration(self.newWindow)

class KidsRegistration:
    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        FirstNameLabel = tk.Label(self.frame, text="First Name")
        FirstNameLabel.pack(side= tk.TOP)
        self.FirstNameEntry  = tk.Entry(self.frame, width=10)
        self.FirstNameEntry.pack()
        self.frame.pack()
        LastNameLable = tk.Label(self.frame, text="Last Name")
        LastNameLable.pack()
        self.LastNameEntry  = tk.Entry(self.frame, width=10)
        self.LastNameEntry.pack()
        self.frame.pack()
        self.Back = tk.Button(self.frame, text = 'Register', width = 25, command = self.print_t)
        self.Back.pack()
        self.frame.pack()
        self.quitButton = tk.Button(self.frame, text = 'Quit', width = 25, command = self.close_windows)
        self.quitButton.pack()
        self.frame.pack()
    def close_windows(self):
        self.master.destroy()
    def print_t(self):
        FirstName = self.FirstNameEntry.get()
        LastName = self.LastNameEntry.get()
        print (FirstName,LastName )
        self.FirstNameEntry.delete(0, len(FirstName))
        self.LastNameEntry.delete(0, len(LastName))
        content = tk.StringVar()
        messagebox.showinfo("OURGUI", "Done!")
        self.close_windows()


def main(): 
    root = tk.Tk()
    app = Demo1(root)
    root.mainloop()

if __name__ == '__main__':
    main()
